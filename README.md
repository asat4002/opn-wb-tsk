# Opn-wb task

### Docker
##### Run tests locally

```shell  
 $ npm run docker-build
 $ npm run docker-run
 ```
Reports will be available in regular folders (Same as for local run)

### Installation
Requirements
- Framework requires [Node.js](https://nodejs.org/)

Install the dependencies

```shell  
$ npm install
```

### Run
##### Run tests locally

```shell  
 $ npm test
 ```

##### Run tests remotely
Run pipeline on gitlabCI
https://gitlab.com/asat4002/opn-wb-tsk/-/pipelines


### Configuration

##### Framework
/configs
- ./playwright.config.ts

### Reporting

- ./playwright-report/index.html
- ./video