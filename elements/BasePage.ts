import {Page} from "@playwright/test";

export abstract class BasePage {
    abstract url: string;

    protected constructor(protected page: Page) {
    }

    async goto() {
        await this.page.goto(this.url);
    }
}