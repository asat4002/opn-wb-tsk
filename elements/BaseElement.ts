import {Locator} from "@playwright/test";

export abstract class BaseElement {
    protected constructor(protected rootElement: Locator) {
    }
}