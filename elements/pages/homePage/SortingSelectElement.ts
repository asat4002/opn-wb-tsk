import {expect, Locator} from "@playwright/test";
import {BaseModalElement} from "../BaseModalElement";

export class SortingSelectElement extends BaseModalElement{

    constructor(rootElement: Locator) {
        super(rootElement);
    }

    async setSorting({locator, text}): Promise<void> {
        await this.rootElement.click();
        await this.rootElement.page().locator(locator).click();
        await expect(this.rootElement).toHaveText(text);
    }
}

export const SortingMenuItems = {
    newest: {
        locator: '[data-testid="newest"]',
        text: 'Newest',
    }
}