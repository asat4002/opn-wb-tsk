import {Locator} from "@playwright/test";
import {BaseModalElement} from "../BaseModalElement";

export class SignupModalElement extends BaseModalElement {
    userNameInput: Locator;
    emailInput: Locator;
    passwordInput: Locator;

    constructor(rootElement: Locator) {
        super(rootElement);
        this.userNameInput = rootElement.locator('[data-testid="username"]');
        this.emailInput = rootElement.locator('[data-testid="email"]');
        this.passwordInput = rootElement.locator('[data-testid="password"]');
    }

    async fill({user, email, password}): Promise<void> {
        await this.userNameInput.type(user);
        await this.emailInput.type(email);
        await this.passwordInput.type(password);
        await this.submit();
    }
}