import {Locator} from "@playwright/test";
import {BaseModalElement} from "../BaseModalElement";

export class UserSettingsModalElement extends BaseModalElement {
    bioInput: Locator;
    locationInput: Locator;

    constructor(rootElement: Locator) {
        super(rootElement);
        this.bioInput = rootElement.locator('#user-profile-bio');
        this.locationInput = rootElement.locator('[data-testid="input-location"]');
    }
}