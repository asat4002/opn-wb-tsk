import {Locator} from "@playwright/test";
import {BaseModalElement} from "../BaseModalElement";

export class CompleteProfileModalElement extends BaseModalElement{
    private locationInput: Locator;
    private bioInput: Locator;

    constructor(rootElement: Locator) {
        super(rootElement);
        this.locationInput =  rootElement.locator('[data-testid="location-input"]');
        this.bioInput =  rootElement.locator('[data-testid="bio"]');
    }

    async fill({bio, location}): Promise<void> {
        await this.locationInput.type(location);
        await this.bioInput.type(bio);
        await this.submit();
    }
}