import {Locator} from "@playwright/test";
import {BaseModalElement} from "../BaseModalElement";

export class CommentElement extends BaseModalElement {
    userName: Locator;
    text: Locator;
    menuButton: Locator;
    deleteMenuItem: Locator;
    message: Locator;

    constructor(rootElement: Locator) {
        super(rootElement);
        this.userName = rootElement.locator('[data-spot-im-class="user-info-username"]');
        this.text = rootElement.locator('[data-spot-im-class="message-text"]');
        this.menuButton = rootElement.locator('[aria-label="Open menu options"]');
        this.deleteMenuItem = rootElement.page().locator('[aria-label="Delete comment"]');
        this.message = rootElement.locator('[data-spot-im-class="message-view"]');
    }

    async delete(): Promise<void> {
        await this.menuButton.click();
        await this.deleteMenuItem.click();
    }
}