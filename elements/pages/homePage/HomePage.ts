import {expect, Locator, Page} from "@playwright/test";
import {BasePage} from "../../BasePage";
import {SignupModalElement} from "./SignupModalElement";
import {CompleteProfileModalElement} from "./CompleteProfileModalElement";
import {SortingSelectElement} from "./SortingSelectElement";
import {BaseModalElement} from "../BaseModalElement";
import {CommentElement} from "./CommentElement";
import {UserSettingsModalElement} from "./UserSettingsModalElement";


export class HomePage extends BasePage {
    url = '/www.spotim.name/Moshe/production/rc_manual_2.html';
    commentsCountTitle: Locator;
    comments: Locator;
    newCommentNoFocusInput: Locator;
    newCommentFocusInput: Locator;
    newCommentUserNameInput: Locator;
    newCommentSendButton: Locator;
    newCommentSignupButton: Locator;
    userSettingMenuButton: Locator;
    logoutMenuItem: Locator;
    guestButtons: Locator;
    userProfileButton: Locator;
    settingsMenuItem: Locator;

    constructor(page: Page) {
        super(page);
        this.commentsCountTitle = page.locator('[data-spot-im-class="comments-count"]');
        this.comments = page.locator('[aria-label="Comment"]');
        this.newCommentNoFocusInput = page.locator('[data-testid="editor container"]');
        this.newCommentFocusInput = page.locator('#spotim-say-config');
        this.newCommentUserNameInput = page.locator('[placeholder="Your Username"]');
        this.newCommentSendButton = page.locator('[aria-label="Publish comment"]');
        this.newCommentSignupButton = page.locator('#spotim-woohoo-action-btn');

        this.userSettingMenuButton = page.locator('[aria-label="User Menu"]');
        this.settingsMenuItem = page.locator('[aria-label="Open my settings"]');
        this.logoutMenuItem = page.locator('[aria-label="Log out from conversation"]');
        this.guestButtons = page.locator('[data-spot-im-class="registration-buttons"]');
        this.userProfileButton = page.locator('.spcv_empty-username');
    }

    async getTitleCommentsCount(): Promise<number> {
        return parseInt(await this.commentsCountTitle.textContent());
    }

    async sendComment({text, user}): Promise<void> {
        await this.newCommentNoFocusInput.click();
        await this.newCommentFocusInput.type(text);
        await this.newCommentUserNameInput.type(user);
        await this.newCommentSendButton.click();
    }

    async signupUserFromGuestComment({user, email, password, bio, location}): Promise<void> {
        await this.newCommentSignupButton.click();
        await this.getSignupModal().fill({user, email, password});
        await this.getCompleteProfileModal().fill({bio, location});
    }

    async openUserSettings(): Promise<void> {
        await this.userSettingMenuButton.click();
        await this.settingsMenuItem.click();
    }

    async logout(): Promise<void> {
        await this.userSettingMenuButton.click();
        await this.logoutMenuItem.click();
    }

    async confirmCommentDeletion(): Promise<void> {
        await Promise.all([
            this.page.waitForResponse('https://api-2-0.spot.im/v1.0.0/moderation/message/*'),
            this.page.waitForTimeout(2000),
            this.getModal().submit()
        ]);
    }

    getSignupModal(): SignupModalElement {
        return new SignupModalElement(this.page.locator('[data-testid="signup-modal"]'));
    }

    getCompleteProfileModal(): CompleteProfileModalElement {
        return new CompleteProfileModalElement(this.page.locator('[data-testid="onboarding-modal"]'));
    }

    getUserSettingsModal(): UserSettingsModalElement {
        return new UserSettingsModalElement(this.page.locator('[data-testid="user-settings-modal"]'));
    }

    getModal(): BaseModalElement {
        return new BaseModalElement(this.page.locator('[data-testid="modal"]'));
    }

    getSortingSelect(): SortingSelectElement {
        return new SortingSelectElement(this.page.locator('#spotim-sort-by'));
    }

    getFirstComment(): CommentElement {
        return new CommentElement(this.page.locator('[aria-label="Comment"]').first());
    }
}