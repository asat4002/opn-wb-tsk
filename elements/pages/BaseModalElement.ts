import {Locator} from "@playwright/test";
import {BaseElement} from "../BaseElement";


export class BaseModalElement extends BaseElement {
    submitButton: Locator;
    closeButton: Locator;

    constructor(rootElement: Locator) {
        super(rootElement);
        this.submitButton = rootElement.locator('[data-testid="submit"],[aria-label*="Confirm"]');
        this.closeButton = rootElement.locator('[title="Close the modal"]');
    }

    submit(): Promise<void> {
        return this.submitButton.first().click();
    }

    close(): Promise<void> {
        return this.closeButton.click();
    }
}