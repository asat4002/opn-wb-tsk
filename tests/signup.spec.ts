import {expect, Page, test} from "@playwright/test";
import {HomePage} from "../elements/pages/homePage/HomePage";
import {SortingMenuItems} from "../elements/pages/homePage/SortingSelectElement";
import {CommentElement} from "../elements/pages/homePage/CommentElement";
import {UserSettingsModalElement} from "../elements/pages/homePage/UserSettingsModalElement";

test.describe('Signup flow', () => {
    let context;
    test.beforeEach(async ({browser}) => {
        context = await browser.newContext({recordVideo: {dir: 'video/signup'}});
    });

    test('comments', async () => {
        const page: Page = await context.newPage();
        const seed = Date.now();
        const testData = {
            text: 'I like candies',
            user: `Autotest user ${seed}`,
            location: `Ašdod, Israel`,
            bio: `Autotest's script ${seed}`,
            email: `auto${seed}@mail.com`,
            password: `password+${seed}`,
        }

        const homePage = new HomePage(page);

        await test.step('Signup', async () => {
            await homePage.goto();
            const initialCommentCount = await homePage.getTitleCommentsCount();
            expect(await homePage.comments.count()).toBeLessThanOrEqual(initialCommentCount);
 
            await homePage.sendComment(testData);
            await expect(homePage.commentsCountTitle).toContainText(`${initialCommentCount + 1}`);
            await homePage.signupUserFromGuestComment(testData)
        });

        await page.reload();

        await test.step('Check saved data', async () => {
            await homePage.getSortingSelect().setSorting(SortingMenuItems.newest)
            const firstComment: CommentElement = homePage.getFirstComment();
            await expect(firstComment.userName).toHaveText(testData.user);
            await expect(firstComment.text).toHaveText(testData.text);
            await homePage.openUserSettings();
            const userSettingsModal: UserSettingsModalElement = homePage.getUserSettingsModal();
            await expect(userSettingsModal.bioInput).toHaveText(testData.bio);
            await expect(userSettingsModal.locationInput).toHaveValue(testData.location);
            await userSettingsModal.close();
        });

        await test.step('Check deletion and logout', async () => {
            const initialCommentCount = await homePage.getTitleCommentsCount();
            const firstComment: CommentElement = homePage.getFirstComment();
            await firstComment.delete();
            await homePage.confirmCommentDeletion();
            await expect(firstComment.message).toHaveText('This message was deleted.');
            await expect.soft(await homePage.getTitleCommentsCount()).toBeLessThan(initialCommentCount);

            await page.reload();

            const firstCommentUpdated: CommentElement = homePage.getFirstComment();
            await expect(firstCommentUpdated.userName).not.toHaveText(testData.user);
            await homePage.logout();
            await expect(homePage.guestButtons).toContainText(`Log in`);
            await expect(homePage.userProfileButton).toContainText(`Guest`);
        });
    });

    test.afterEach(async () => {
        await context.close();
    })
});