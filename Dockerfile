FROM mcr.microsoft.com/playwright:v1.21.1-focal
USER root

WORKDIR /e2e

COPY . ./

RUN npm install
RUN npx playwright install

CMD [ "npm", "run", "test-ci" ]